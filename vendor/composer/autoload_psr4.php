<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Ublaboo\\DataGrid\\' => array($vendorDir . '/ublaboo/datagrid/src'),
    'Symfony\\Polyfill\\Php80\\' => array($vendorDir . '/symfony/polyfill-php80'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Polyfill\\Intl\\Normalizer\\' => array($vendorDir . '/symfony/polyfill-intl-normalizer'),
    'Symfony\\Polyfill\\Intl\\Grapheme\\' => array($vendorDir . '/symfony/polyfill-intl-grapheme'),
    'Symfony\\Polyfill\\Ctype\\' => array($vendorDir . '/symfony/polyfill-ctype'),
    'Symfony\\Component\\String\\' => array($vendorDir . '/symfony/string'),
    'Symfony\\Component\\PropertyInfo\\' => array($vendorDir . '/symfony/property-info'),
    'Symfony\\Component\\PropertyAccess\\' => array($vendorDir . '/symfony/property-access'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-message/src'),
    'PHPStan\\' => array($vendorDir . '/phpstan/phpstan-nette/src'),
    'Nextras\\Orm\\' => array($vendorDir . '/nextras/orm/src'),
    'Nextras\\Dbal\\' => array($vendorDir . '/nextras/dbal/src'),
    'GuzzleHttp\\Psr7\\' => array($vendorDir . '/guzzlehttp/psr7/src'),
    'Doctrine\\Common\\Lexer\\' => array($vendorDir . '/doctrine/lexer/lib/Doctrine/Common/Lexer'),
    'Doctrine\\Common\\Cache\\' => array($vendorDir . '/doctrine/cache/lib/Doctrine/Common/Cache'),
    'Doctrine\\Common\\Annotations\\' => array($vendorDir . '/doctrine/annotations/lib/Doctrine/Common/Annotations'),
    'Contributte\\Psr7\\' => array($vendorDir . '/contributte/psr7-http-message/src'),
    'Contributte\\PhpDoc\\' => array($vendorDir . '/contributte/phpdoc/src'),
    'Contributte\\DI\\' => array($vendorDir . '/contributte/di/src'),
    'Contributte\\Application\\' => array($vendorDir . '/contributte/application/src'),
    'App\\' => array($baseDir . '/app'),
    'Apitte\\Core\\' => array($vendorDir . '/apitte/core/src'),
);
