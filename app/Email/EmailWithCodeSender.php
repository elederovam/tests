<?php declare(strict_types=1);

namespace App\Email;

use Tracy\Debugger;

class EmailWithCodeSender
{
    private EmailSender $emailSender;

    private string $template;

    /**
     * @param EmailSender $emailSender
     */
    public function __construct(EmailSender $emailSender)
    {
        $this->emailSender = $emailSender;
        $this->template = __DIR__ . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'emailWithCode.latte';
    }

    /**
     * Send email with the given code to the given email address.
     *
     * @param string $emailTo
     * @param string $code
     */
    public function sendEmail(string $emailTo, string $code): void
    {
        $params = ['code' => $code];
        Debugger::barDump($code);
        $this->emailSender->sendEmail($emailTo, $this->template, $params);
    }
}
