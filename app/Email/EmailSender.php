<?php declare(strict_types=1);

namespace App\Email;

use Latte\Engine;
use Nette\Bridges\ApplicationLatte\ILatteFactory;
use Nette\Bridges\ApplicationLatte\UIMacros;
use Nette\Mail\Mailer;
use Nette\Mail\Message;
use Tracy\Debugger;
use Tracy\ILogger;

class EmailSender
{
    private Mailer $mailer;

    private Engine $latte;

    private const EMAIL_FROM = 'elederovam@seznam.cz';

    /**
     * @param ILatteFactory $latteFactory
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer, ILatteFactory $latteFactory)
    {
        $this->mailer = $mailer;

        $latte = $latteFactory->create();
        UIMacros::install($latte->getCompiler());
        $this->latte = $latte;
    }

    /**
     * Send email to address $to using given template with given parameters.
     *
     * @param string $to
     * @param string $template
     * @param array<string, mixed> $params
     */
    public function sendEmail(string $to, string $template, array $params): void
    {
        $mail = new Message();
        $mail->setFrom($this::EMAIL_FROM);
        $mail->addTo($to);
        $mail->setHtmlBody($this->latte->renderToString($template, $params));

        $this->mailer->send($mail);

        Debugger::log('Send email to ' . $to, ILogger::INFO);
    }
}
