<?php declare(strict_types=1);

namespace App\ApiModule\V1\Controllers;

use Apitte\Core\Annotation\Controller\ControllerPath;
use Apitte\Core\Annotation\Controller\Method;
use Apitte\Core\Annotation\Controller\Path;
use Apitte\Core\Http\ApiRequest;
use Apitte\Core\Http\ApiResponse;
use Apitte\Core\UI\Controller\IController;
use App\ApiModule\Exceptions\InvalidDataFormatException;
use App\Dto\TestQuestionAnswerApiDto;
use App\Facade\Exceptions\EntityDoesNotExistException;
use App\Facade\ITestFacade;
use Tracy\Debugger;

/**
 * @ControllerPath("/api/v1/question")
 */
class QuestionController implements IController
{
    public ITestFacade $testFacade;

    /**
     * @param ITestFacade $testFacade
     */
    public function __construct(ITestFacade $testFacade)
    {
        $this->testFacade = $testFacade;
    }

    /**
     * Post previous answer JSON in format:
    {
        "$schema": "http://json-schema.org/draft/2019-09/schema",
        "title": "Answer",
        "type": "object",
        "required": ["hasAnswer", "code"],
        "properties": {,
            "code": {
                "type": "string",
            },
            "hasAnswer": {
                "type": "boolean",
                "description": "If true, must have all other properties."
            },
            "answer": {
                "type": "object",
                "properties": {
                    "questionRank": {
                        "type": "number",
                    },
                    "answerRank": {
                        "type": "number",
                    }
                }
            }
        }
    }
     * If there is none previous question (so that no previous answer) is hasAnswer set to false.
     *
     * In response is sent JSON with random next question in format:
    {
        "$schema": "http://json-schema.org/draft/2019-09/schema",
        "title": "Question",
        "type": "object",
        "required": ["hasQuestion", "validCode"],
        "properties": {
            "validCode": {
                "type": "boolean",
                "description": "False if requested test code is invalid."
            },
            "hasQuestion": {
                "type": "boolean",
                "description": "If true, must have all other properties."
            },
            "question": {
                "type": "object",
                "properties": {
                    "text": {
                        "type": "string",
                    },
                    "rank": {
                        "type": "number",
                        "description": "number 1–10"
                    },
                    "numberOfQuestions": {
                        "type": "number",
                        "description": "number od all questions"
                    },
                    "answer1": {
                        "type": "string"
                    }
                    "answer2": {
                        "type": "string"
                    }
                    "answer3": {
                        "type": "string"
                    }
                    "answer4": {
                        "type": "string"
                    }
                }
            }
        }
    }
     * If the test is completed property hasQuestion is set to false and no question is sent.
     * If sent test code is invalid validCode is set to false.
     *
     * @Path("/")
     * @Method("POST")
     * @param ApiRequest $request
     * @param ApiResponse $response
     * @return ApiResponse
     */
    public function index(ApiRequest $request, ApiResponse $response): ApiResponse
    {
        $requestData = $request->getJsonBody();
//        if ($previousAnswer[$requestData['code']) todo validate
        if ($requestData['hasAnswer']) {
            try {
                $previousAnswer = $requestData['answer'];
                $dto = new TestQuestionAnswerApiDto();
                $dto->setQuestionRank((int) $previousAnswer[TestQuestionAnswerApiDto::QUESTION_RANK_NAME]);
                $dto->setAnswerRank((int) $previousAnswer[TestQuestionAnswerApiDto::ANSWER_RANK_NAME]);
            } catch (\Throwable $exception) {
                Debugger::log($exception, Debugger::ERROR);
                throw new InvalidDataFormatException('Request has invalid JSON with answer.', 0, $exception);
            }
        }
        else {
            $dto = null;
        }

        $validCode = true;
        try {
            $nextQuestion = $this->testFacade->processAnswerAndGetNextQuestion($dto, $requestData['code']);
        } catch (EntityDoesNotExistException $exception) {
            $validCode = false;
        } catch (\Throwable $exception) {
            Debugger::log($exception, Debugger::ERROR);
            $nextQuestion = null;
        }

        if ($validCode) {
            if ($nextQuestion === null) {
                $responseData = [
                    'validCode' => true,
                    'hasQuestion' => false,
                ];
            }
            else {
                $responseData = [
                    'validCode' => true,
                    'hasQuestion' => true,
                    'question' => $nextQuestion,
                ];
            }
        }
        else {
            $responseData = [
                'validCode' => false,
                'hasQuestion' => false,
            ];
        }

        $response->writeJsonBody($responseData);
        return $response;
    }
}
