<?php declare(strict_types=1);

namespace App\ApiModule\V1\Controllers;

use Apitte\Core\Annotation\Controller\ControllerPath;
use Apitte\Core\Annotation\Controller\Method;
use Apitte\Core\Annotation\Controller\Path;
use Apitte\Core\Http\ApiRequest;
use Apitte\Core\Http\ApiResponse;
use Apitte\Core\UI\Controller\IController;
use App\Facade\Exceptions\EntityDoesNotExistException;
use App\Facade\ITestFacade;
use Tracy\Debugger;

/**
 * @ControllerPath("/api/v1/result")
 */
class ResultController implements IController
{
    public ITestFacade $testFacade;

    /**
     * @param ITestFacade $testFacade
     */
    public function __construct(ITestFacade $testFacade)
    {
        $this->testFacade = $testFacade;
    }

    /**
     * Get test result in format:
    {
        "$schema": "http://json-schema.org/draft/2019-09/schema",
        "title": "TestResult",
        "type": "object",
        "required": ["validCode"],
        "properties": {
            "validCode": {
                "type": "boolean",
                "description": "False if requested test code is invalid."
            },
            "result": {
                "type": "object",
                "properties": {
                    "score": {
                        "type": "number",
                        "description": "Number of correct answers."
                    },
                    "questions": {
                        "type": "array",
                        "items": {
                            "type": "object",
                            "properties": {
                                "questionRank": {
                                    "type": "number"
                                },
                                "questionText": {
                                    "type": "string"
                                },
                                "answers": {
                                    "type": "array",
                                    "description": "four answers",
                                    "items": {
                                        "type": "object",
                                        "properties": {
                                            "answerText": {
                                                "type": "text"
                                            },
                                            "isCorrect": {
                                                "type": "boolean"
                                            },
                                            "isSelected": {
                                                "type": "boolean"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
     *
     * @Path("/")
     * @Method("POST")
     * @param ApiRequest $request
     * @param ApiResponse $response
     * @return ApiResponse
     * @throws \Throwable
     */
    public function index(ApiRequest $request, ApiResponse $response): ApiResponse
    {
        $code = $request->getJsonBody();

        $validCode = true;
        try {
            $testResult = $this->testFacade->getTestResult($code);
        } catch (EntityDoesNotExistException $exception) {
            $validCode = false;
        } catch (\Throwable $exception) {
            Debugger::log($exception, Debugger::ERROR);
            throw $exception;
        }

        if ($validCode) {
            $responseData = [
                'validCode' => true,
                'result' => $testResult,
            ];
        }
        else {
            $responseData = [
                'validCode' => false,
            ];
        }

        $response->writeJsonBody($responseData);
        return $response;
    }
}
