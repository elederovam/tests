<?php declare(strict_types=1);

namespace App\Facade;

use App\Dto\TestQuestionAnswerApiDto;
use App\Dto\TestQuestionApiDto;
use App\Dto\TestResultApiDto;
use App\Dto\TestResultQuestionAnswerApiDto;
use App\Dto\TestResultQuestionApiDto;
use App\Email\EmailWithCodeSender;
use App\Facade\Exceptions\EntityDoesNotExistException;
use App\Model\Entity\Question;
use App\Model\Entity\Test;
use App\Model\Entity\TestQuestion;
use App\Model\Entity\TestQuestionAnswer;
use App\Model\Orm;
use App\Model\Repository\QuestionRepository;
use App\Model\Repository\TestQuestionAnswerRepository;
use App\Model\Repository\TestRepository;
use Nette\Utils\Random;
use Nextras\Dbal\Connection;
use Tracy\Debugger;

class TestFacade implements ITestFacade
{
    private const NUMBER_OF_QUESTIONS = 10;

    public Orm $orm;

    private EmailWithCodeSender $emailWithCodeSender;

    private Connection $connection;

    /**
     * @param Orm $orm
     * @param EmailWithCodeSender $emailWithCodeSender
     * @param Connection $connection
     */
    public function __construct(Orm $orm, EmailWithCodeSender $emailWithCodeSender, Connection $connection)
    {
        $this->orm = $orm;
        $this->emailWithCodeSender = $emailWithCodeSender;
        $this->connection = $connection;
    }

    /**
     * Generate random code, create new test in database and send email with the code to the given address.
     *
     * @param string $emailTo
     */
    public function sendEmailWithCode(string $emailTo): void
    {
        $code = Random::generate(6, '0-9');
        $test = new Test();
        $test->code = $code;
        $this->orm->test->persistAndFlush($test);

        $this->emailWithCodeSender->sendEmail($emailTo, $code);
    }

    /**
     * Save answer to database (if it is not null) and return next random question with random shuffled answers.
     * If the test is completed (there should not be next question) null is returned.
     *
     * @param TestQuestionAnswerApiDto|null $answerApiDto
     * @param string $code
     * @return TestQuestionApiDto|null
     * @throws \Nextras\Dbal\QueryException
     */
    public function processAnswerAndGetNextQuestion(?TestQuestionAnswerApiDto $answerApiDto, string $code): ?TestQuestionApiDto
    {
        if ($answerApiDto !== null) {
            $query = $this->connection->query('SELECT * FROM test 
                INNER JOIN test_question ON test_question.test_id = test.id 
                INNER JOIN test_question_answer ON test_question_answer.test_question_id = test_question.id
                WHERE test.code = %s AND test_question.rank = %i;',
                $code,
                $answerApiDto->getQuestionRank(),
            );
            $rows = $query->fetchAll();
            foreach ($rows as $row) {
                /** @var TestQuestionAnswer $testQuestionAnswer */
                $testQuestionAnswer = $this->orm->getRepository(TestQuestionAnswerRepository::class)
                    ->hydrateEntity($row->toArray());
                if ($testQuestionAnswer->rank === $answerApiDto->getAnswerRank()) {
                    $testQuestionAnswer->selected = true;
                } else {
                    $testQuestionAnswer->selected = false;
                }
                $this->orm->persist($testQuestionAnswer);
            }
        }

        /** @var Test|null $test */
        $test = $this->orm->getRepository(TestRepository::class)->getBy(['code' => $code]);
        if ($test === null) {
            throw new EntityDoesNotExistException(sprintf('Test with code %s does not exist.', $code));
        }

        $previousRank = count($test->testQuestions);
        if ($previousRank === self::NUMBER_OF_QUESTIONS) {
            return null;
        }

        $allQuestionsQuery = $this->orm->getRepository(QuestionRepository::class)->findAll()->orderBy('id');
        $questionsNumber = $allQuestionsQuery->count();
        /** @var int $number */
        $number = array_rand(range(0, $questionsNumber - 1));
        /** @var Question $nextQuestion */
        $nextQuestion = $allQuestionsQuery->limitBy(1, $number)->fetch();

        $testQuestion = new TestQuestion();
        $testQuestion->test = $test;
        $testQuestion->question = $nextQuestion;
        $testQuestion->rank = $previousRank + 1;
        $this->orm->persist($testQuestion);

        $i = 1;
        $testAnswers = [];
        $answers = [];
        foreach ($nextQuestion->answers as $answer) {
            $answers[] = $answer;
        }
        shuffle($answers);
        foreach ($answers as $answer) {
            $testAnswer = new TestQuestionAnswer();
            $testAnswer->testQuestion = $testQuestion;
            $testAnswer->answer = $answer;
            $testAnswer->rank = $i;
            $testAnswer->selected = false;

            $testAnswers[] = $testAnswer;
            $this->orm->persist($testAnswer);
            ++$i;
        }

        $this->orm->flush();

        $testQuestionApiDto = new TestQuestionApiDto();
        $testQuestionApiDto->setNumberOfQuestions(self::NUMBER_OF_QUESTIONS);
        $testQuestionApiDto->setText($nextQuestion->text);
        $testQuestionApiDto->setRank($testQuestion->rank);
        $testQuestionApiDto->setAnswer1($testAnswers[0]->answer ? $testAnswers[0]->answer->text : '[Smazaná odpověď]');
        $testQuestionApiDto->setAnswer2($testAnswers[1]->answer ? $testAnswers[1]->answer->text : '[Smazaná odpověď]');
        $testQuestionApiDto->setAnswer3($testAnswers[2]->answer ? $testAnswers[2]->answer->text : '[Smazaná odpověď]');
        $testQuestionApiDto->setAnswer4($testAnswers[3]->answer ? $testAnswers[3]->answer->text : '[Smazaná odpověď]');
        return $testQuestionApiDto;
    }

    /**
     * Get from database result of test with the given code.
     *
     * @param string $code
     * @return TestResultApiDto
     */
    public function getTestResult(string $code): TestResultApiDto
    {
        /** @var Test|null $test */
        $test = $this->orm->getRepository(TestRepository::class)->getBy(['code' => $code]);
        if ($test === null) {
            throw new EntityDoesNotExistException(sprintf('Test with code %s does not exist.', $code));
        }

        $testResultDto = new TestResultApiDto();
        $score = 0;
        foreach ($test->testQuestions as $testQuestion) {
            $testQuestionDto = new TestResultQuestionApiDto();
            $testQuestionDto->setQuestionRank($testQuestion->rank);
            $testQuestionDto->setQuestionText($testQuestion->question ? $testQuestion->question->text : '[Smazaná otázka]');

            foreach ($testQuestion->testQuestionAnswers as $testQuestionAnswer) {
                $testQuestionAnswerDto = new TestResultQuestionAnswerApiDto();
                $testQuestionAnswerDto->setAnswerText($testQuestionAnswer->answer ? $testQuestionAnswer->answer->text : '-');
                $testQuestionAnswerDto->setIsCorrect($testQuestionAnswer->answer ? $testQuestionAnswer->answer->correct : false);
                $testQuestionAnswerDto->setIsSelected($testQuestionAnswer->selected);
                if ($testQuestionAnswerDto->isCorrect() && $testQuestionAnswerDto->isSelected()) {
                    ++$score;
                }

                $testQuestionDto->addAnswer($testQuestionAnswerDto);
            }

            $testResultDto->addQuestion($testQuestionDto);
        }
        $testResultDto->setScore($score);

        return $testResultDto;
    }
}
