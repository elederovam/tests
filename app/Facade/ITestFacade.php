<?php declare(strict_types=1);

namespace App\Facade;

use App\Dto\TestQuestionAnswerApiDto;
use App\Dto\TestQuestionApiDto;
use App\Dto\TestResultApiDto;

interface ITestFacade
{
    /**
     * Generate random code, create new test in database and send email with the code to the given address.
     *
     * @param string $emailTo
     */
    public function sendEmailWithCode(string $emailTo): void;

    /**
     * Save answer to database (if it is not null) and return next random question with random shuffled answers.
     * If the test is completed (there should not be next question) is returned null.
     *
     * @param TestQuestionAnswerApiDto|null $answerApiDto
     * @param string $code
     * @return TestQuestionApiDto|null
     * @throws \Nextras\Dbal\QueryException
     */
    public function processAnswerAndGetNextQuestion(?TestQuestionAnswerApiDto $answerApiDto, string $code): ?TestQuestionApiDto;

    /**
     * Get from database result of test with the given code.
     *
     * @param string $code
     * @return TestResultApiDto
     */
    public function getTestResult(string $code): TestResultApiDto;
}
