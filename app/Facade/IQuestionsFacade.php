<?php declare(strict_types=1);

namespace App\Facade;

use App\Dto\QuestionDto;
use App\Dto\QuestionDtoWithAnswerIds;
use App\Facade\Exceptions\EntityDoesNotExistException;
use Nextras\Orm\Collection\ICollection;

interface IQuestionsFacade
{
    /**
     * Get question with given ID.
     *
     * @param int $id
     * @return QuestionDto
     * @throws EntityDoesNotExistException if the question does not exist
     */
    public function getQuestionDto(int $id): QuestionDto;

    /**
     * Get question with given ID together with Ids of its answers.
     *
     * @param int $id
     * @return QuestionDtoWithAnswerIds
     * @throws EntityDoesNotExistException if the question does not exist
     */
    public function getQuestionDtoWithAnswerIds(int $id): QuestionDtoWithAnswerIds;

    /**
     * Get Nextras collection with all questions.
     *
     * @return ICollection
     */
    public function getQuestionsCollection(): ICollection;

    /**
     * Create Question and Answer entities according to the Dto and store them in database. Return ID of new
     * created question.
     *
     * @param QuestionDto $questionDto
     * @return int
     */
    public function createQuestion(QuestionDto $questionDto): int;

    /**
     * Update question with given ID according to the given Dto.
     *
     * @param int $id
     * @param QuestionDtoWithAnswerIds $questionDto
     */
    public function updateQuestion(int $id, QuestionDtoWithAnswerIds $questionDto): void;

    /**
     * Delete question with given ID and its answers.
     *
     * @param int $id
     */
    public function deleteQuestion(int $id): void;
}
