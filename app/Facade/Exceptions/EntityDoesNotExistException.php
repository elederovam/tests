<?php declare(strict_types=1);

namespace App\Facade\Exceptions;

class EntityDoesNotExistException extends \RuntimeException
{

}
