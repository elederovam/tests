<?php declare(strict_types=1);

namespace App\Facade;

use App\Dto\QuestionDto;
use App\Dto\QuestionDtoWithAnswerIds;
use App\Facade\Exceptions\EntityDoesNotExistException;
use App\Model\Entity\Answer;
use App\Model\Entity\Question;
use App\Model\Orm;
use Nextras\Orm\Collection\ICollection;
use Tracy\Debugger;

class QuestionsFacade implements IQuestionsFacade
{
    public Orm $orm;

    /**
     * @param Orm $orm
     */
    public function __construct(Orm $orm)
    {
        $this->orm = $orm;
    }

    /**
     * Get question with given ID.
     *
     * @param int $id
     * @return QuestionDto
     * @throws EntityDoesNotExistException if the question does not exist
     */
    public function getQuestionDto(int $id): QuestionDto
    {
        /** @var Question|null $question */
        $question = $this->orm->question->getById($id);
        if ($question === null) {
            throw new EntityDoesNotExistException(sprintf('Question with ID %d does not exist.', $id));
        }

        $questionDto = new QuestionDto();
        $questionDto->setQuestion($question->text);
        $correctAnswersCount = 0;
        foreach ($question->answers as $answer) {
            if ($answer->correct) {
                $questionDto->setCorrectAnswer($answer->text);
                ++$correctAnswersCount;
            }
            else {
                $questionDto->addWrongAnswer($answer->text);
            }
        }
        if ($correctAnswersCount === 0) {
            Debugger::log(sprintf('Question with ID %d has no correct answer.', Debugger::ERROR));
        }
        elseif ($correctAnswersCount > 1) {
            Debugger::log(sprintf('Question with ID %d has more correct answers.', Debugger::ERROR));
        }

        return $questionDto;
    }

    /**
     * Get question with given ID together with Ids of its answers.
     *
     * @param int $id
     * @return QuestionDtoWithAnswerIds
     * @throws EntityDoesNotExistException if the question does not exist
     */
    public function getQuestionDtoWithAnswerIds(int $id): QuestionDtoWithAnswerIds
    {
        /** @var Question|null $question */
        $question = $this->orm->question->getById($id);
        if ($question === null) {
            throw new EntityDoesNotExistException(sprintf('Question with ID %d does not exist.', $id));
        }

        $questionDto = new QuestionDtoWithAnswerIds();
        $questionDto->setQuestion($question->text);
        $correctAnswersCount = 0;
        foreach ($question->answers as $answer) {
            if ($answer->correct) {
                $questionDto->setCorrectAnswer($answer->text);
                $questionDto->setCorrectAnswerId($answer->id);
                ++$correctAnswersCount;
            }
            else {
                $questionDto->addWrongAnswer($answer->id, $answer->text);
            }
        }
        if ($correctAnswersCount === 0) {
            Debugger::log(sprintf('Question with ID %d has no correct answer.', Debugger::ERROR));
        }
        elseif ($correctAnswersCount > 1) {
            Debugger::log(sprintf('Question with ID %d has more correct answers.', Debugger::ERROR));
        }

        return $questionDto;
    }

    /**
     * Get Nextras collection with all questions.
     *
     * @return ICollection
     */
    public function getQuestionsCollection(): ICollection
    {
        return $this->orm->question->findAll();
    }

    /**
     * Create Question and Answer entities according to the Dto and store them in database. Return ID of new
     * created question.
     *
     * @param QuestionDto $questionDto
     * @return int
     */
    public function createQuestion(QuestionDto $questionDto): int
    {
        $question = new Question();
        $question->text = $questionDto->getQuestion();
        $this->orm->question->persist($question);

        $answer = new Answer();
        $answer->text = $questionDto->getCorrectAnswer();
        $answer->correct = true;
        $answer->question = $question;
        $this->orm->answer->persist($answer);

        foreach ($questionDto->getWrongAnswers() as $wrongAnswer) {
            $answer = new Answer();
            $answer->text = $wrongAnswer;
            $answer->correct = false;
            $answer->question = $question;
            $this->orm->answer->persist($answer);
        }

        $this->orm->flush();
        return $question->id;
    }

    /**
     * Update question with given ID according to the given Dto.
     *
     * @param int $id
     * @param QuestionDtoWithAnswerIds $questionDto
     * @throws EntityDoesNotExistException if the question does not exist
     */
    public function updateQuestion(int $id, QuestionDtoWithAnswerIds $questionDto): void
    {
        /** @var Question|null $question */
        $question = $this->orm->question->getById($id);
        if ($question === null) {
            throw new EntityDoesNotExistException(sprintf('Question with ID %d does not exist.', $id));
        }
        $question->text = $questionDto->getQuestion();
        $this->orm->persist($question);

        $correctAnswer = $this->orm->answer->getById($questionDto->getCorrectAnswerId());
        if ($correctAnswer === null) {
            throw new EntityDoesNotExistException(sprintf('Answer with ID %d does not exist.', $questionDto->getCorrectAnswerId()));
        }
        $correctAnswer->text = $questionDto->getCorrectAnswer();
        $this->orm->persist($correctAnswer);

        foreach ($questionDto->getWrongAnswers() as $wrongAnswerId => $wrongAnswerText) {
            $wrongAnswer = $this->orm->answer->getById($wrongAnswerId);
            if ($wrongAnswer === null) {
                throw new EntityDoesNotExistException(sprintf('Answer with ID %d does not exist.', $wrongAnswerId));
            }
            $wrongAnswer->text = $wrongAnswerText;
            $this->orm->persist($wrongAnswer);
        }

        $this->orm->flush();
    }

    /**
     * Delete question with given ID and its answers.
     *
     * @param int $id
     */
    public function deleteQuestion(int $id): void
    {
        $question = $this->orm->question->getById($id);
        if ($question === null) {
            Debugger::log(sprintf('Question with ID %d should be deleted, but it does not exist.', $id), Debugger::ERROR);
            return;
        }
        $this->orm->remove($question);
        $this->orm->flush();
    }
}
