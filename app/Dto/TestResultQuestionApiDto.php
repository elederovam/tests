<?php declare(strict_types=1);

namespace App\Dto;

class TestResultQuestionApiDto implements \JsonSerializable
{
    private int $questionRank;

    private string $questionText;

    /**
     * @var TestResultQuestionAnswerApiDto[]
     */
    private array $answers = [];

    /**
     * @return int
     */
    public function getQuestionRank(): int
    {
        return $this->questionRank;
    }

    /**
     * @param int $questionRank
     */
    public function setQuestionRank(int $questionRank): void
    {
        $this->questionRank = $questionRank;
    }

    /**
     * @return string
     */
    public function getQuestionText(): string
    {
        return $this->questionText;
    }

    /**
     * @param string $questionText
     */
    public function setQuestionText(string $questionText): void
    {
        $this->questionText = $questionText;
    }

    /**
     * @return TestResultQuestionAnswerApiDto[]
     */
    public function getAnswers(): array
    {
        return $this->answers;
    }

    /**
     * @param TestResultQuestionAnswerApiDto[] $answers
     */
    public function setAnswers(array $answers): void
    {
        $this->answers = $answers;
    }

    /**
     * @param TestResultQuestionAnswerApiDto $testResultQuestionAnswerApiDto
     */
    public function addAnswer(TestResultQuestionAnswerApiDto $testResultQuestionAnswerApiDto): void
    {
        $this->answers[] = $testResultQuestionAnswerApiDto;
    }

    public function jsonSerialize()
    {
        return [
            'questionRank' => $this->questionRank,
            'questionText' => $this->questionText,
            'answers' => $this->answers,
        ];
    }
}
