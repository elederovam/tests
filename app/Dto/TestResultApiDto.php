<?php declare(strict_types=1);

namespace App\Dto;

class TestResultApiDto implements \JsonSerializable
{
    private int $score;

    /**
     * @var TestResultQuestionApiDto[]
     */
    private array $questions = [];

    /**
     * @return int
     */
    public function getScore(): int
    {
        return $this->score;
    }

    /**
     * @param int $score
     */
    public function setScore(int $score): void
    {
        $this->score = $score;
    }

    /**
     * @return TestResultQuestionApiDto[]
     */
    public function getQuestions(): array
    {
        return $this->questions;
    }

    /**
     * @param TestResultQuestionApiDto[] $questions
     */
    public function setQuestions(array $questions): void
    {
        $this->questions = $questions;
    }

    /**
     * @param TestResultQuestionApiDto $testResultQuestionApiDto
     */
    public function addQuestion(TestResultQuestionApiDto $testResultQuestionApiDto): void
    {
        $this->questions[] = $testResultQuestionApiDto;
    }

    public function jsonSerialize()
    {
        return [
            'score' => $this->score,
            'questions' => $this->questions,
        ];
    }
}
