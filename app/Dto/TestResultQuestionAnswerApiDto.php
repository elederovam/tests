<?php declare(strict_types=1);

namespace App\Dto;

class TestResultQuestionAnswerApiDto implements \JsonSerializable
{
    private string $answerText;

    private bool $isCorrect;

    private bool $isSelected;

    /**
     * @return string
     */
    public function getAnswerText(): string
    {
        return $this->answerText;
    }

    /**
     * @param string $answerText
     */
    public function setAnswerText(string $answerText): void
    {
        $this->answerText = $answerText;
    }

    /**
     * @return bool
     */
    public function isCorrect(): bool
    {
        return $this->isCorrect;
    }

    /**
     * @param bool $isCorrect
     */
    public function setIsCorrect(bool $isCorrect): void
    {
        $this->isCorrect = $isCorrect;
    }

    /**
     * @return bool
     */
    public function isSelected(): bool
    {
        return $this->isSelected;
    }

    /**
     * @param bool $isSelected
     */
    public function setIsSelected(bool $isSelected): void
    {
        $this->isSelected = $isSelected;
    }

    public function jsonSerialize()
    {
        return [
            'answerText' => $this->answerText,
            'isCorrect' => $this->isCorrect,
            'isSelected' => $this->isSelected,
        ];
    }
}
