<?php declare(strict_types=1);

namespace App\Dto;

class TestQuestionApiDto implements \JsonSerializable
{
    private string $text;

    private int $rank;

    private int $numberOfQuestions;

    private string $answer1;

    private string $answer2;

    private string $answer3;

    private string $answer4;

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return int
     */
    public function getRank(): int
    {
        return $this->rank;
    }

    /**
     * @param int $rank
     */
    public function setRank(int $rank): void
    {
        $this->rank = $rank;
    }

    /**
     * @return int
     */
    public function getNumberOfQuestions(): int
    {
        return $this->numberOfQuestions;
    }

    /**
     * @param int $numberOfQuestions
     */
    public function setNumberOfQuestions(int $numberOfQuestions): void
    {
        $this->numberOfQuestions = $numberOfQuestions;
    }

    /**
     * @return string
     */
    public function getAnswer1(): string
    {
        return $this->answer1;
    }

    /**
     * @param string $answer1
     */
    public function setAnswer1(string $answer1): void
    {
        $this->answer1 = $answer1;
    }

    /**
     * @return string
     */
    public function getAnswer2(): string
    {
        return $this->answer2;
    }

    /**
     * @param string $answer2
     */
    public function setAnswer2(string $answer2): void
    {
        $this->answer2 = $answer2;
    }

    /**
     * @return string
     */
    public function getAnswer3(): string
    {
        return $this->answer3;
    }

    /**
     * @param string $answer3
     */
    public function setAnswer3(string $answer3): void
    {
        $this->answer3 = $answer3;
    }

    /**
     * @return string
     */
    public function getAnswer4(): string
    {
        return $this->answer4;
    }

    /**
     * @param string $answer4
     */
    public function setAnswer4(string $answer4): void
    {
        $this->answer4 = $answer4;
    }

    public function jsonSerialize()
    {
        return [
            'text' => $this->text,
            'rank' => $this->rank,
            'numberOfQuestions' => $this->numberOfQuestions,
            'answer1' => $this->answer1,
            'answer2' => $this->answer2,
            'answer3' => $this->answer3,
            'answer4' => $this->answer4,
        ];
    }
}
