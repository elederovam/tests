<?php declare(strict_types=1);

namespace App\Dto;

class QuestionDtoWithAnswerIds
{
    private string $question;

    private int $correctAnswerId;

    private string $correctAnswer;

    /** @var array<int, string> */
    private array $wrongAnswers = [];

    /**
     * @return string
     */
    public function getQuestion(): string
    {
        return $this->question;
    }

    /**
     * @param string $question
     */
    public function setQuestion(string $question): void
    {
        $this->question = $question;
    }

    /**
     * @return int
     */
    public function getCorrectAnswerId(): int
    {
        return $this->correctAnswerId;
    }

    /**
     * @param int $correctAnswerId
     */
    public function setCorrectAnswerId(int $correctAnswerId): void
    {
        $this->correctAnswerId = $correctAnswerId;
    }

    /**
     * @return string
     */
    public function getCorrectAnswer(): string
    {
        return $this->correctAnswer;
    }

    /**
     * @param string $correctAnswer
     */
    public function setCorrectAnswer(string $correctAnswer): void
    {
        $this->correctAnswer = $correctAnswer;
    }

    /**
     * @return array<int, string>
     */
    public function getWrongAnswers(): array
    {
        return $this->wrongAnswers;
    }

    /**
     * @param array<int, string> $wrongAnswers
     */
    public function setWrongAnswers(array $wrongAnswers): void
    {
        $this->wrongAnswers = $wrongAnswers;
    }

    /**
     * @param int $id
     * @param string $wrongAnswer
     */
    public function addWrongAnswer(int $id, string $wrongAnswer): void
    {
        $this->wrongAnswers[$id] = $wrongAnswer;
    }
}
