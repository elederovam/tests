<?php declare(strict_types=1);

namespace App\Dto;

class TestQuestionAnswerApiDto
{
    public const QUESTION_RANK_NAME = 'questionRank';
    public const ANSWER_RANK_NAME = 'answerRank';

    private int $questionRank;

    private int $answerRank;

    /**
     * @return int
     */
    public function getQuestionRank(): int
    {
        return $this->questionRank;
    }

    /**
     * @param int $questionRank
     */
    public function setQuestionRank(int $questionRank): void
    {
        $this->questionRank = $questionRank;
    }

    /**
     * @return int
     */
    public function getAnswerRank(): int
    {
        return $this->answerRank;
    }

    /**
     * @param int $answerRank
     */
    public function setAnswerRank(int $answerRank): void
    {
        $this->answerRank = $answerRank;
    }
}
