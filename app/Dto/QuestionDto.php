<?php declare(strict_types=1);

namespace App\Dto;

class QuestionDto
{
    private string $question;

    private string $correctAnswer;

    /** @var string[] */
    private array $wrongAnswers = [];

    /**
     * @return string
     */
    public function getQuestion(): string
    {
        return $this->question;
    }

    /**
     * @param string $question
     */
    public function setQuestion(string $question): void
    {
        $this->question = $question;
    }

    /**
     * @return string
     */
    public function getCorrectAnswer(): string
    {
        return $this->correctAnswer;
    }

    /**
     * @param string $correctAnswer
     */
    public function setCorrectAnswer(string $correctAnswer): void
    {
        $this->correctAnswer = $correctAnswer;
    }

    /**
     * @return string[]
     */
    public function getWrongAnswers(): array
    {
        return $this->wrongAnswers;
    }

    /**
     * @param string[] $wrongAnswers
     */
    public function setWrongAnswers(array $wrongAnswers): void
    {
        $this->wrongAnswers = $wrongAnswers;
    }

    /**
     * @param string $wrongAnswer
     */
    public function addWrongAnswer(string $wrongAnswer): void
    {
        $this->wrongAnswers[] = $wrongAnswer;
    }
}
