<?php declare(strict_types=1);

namespace App\Model;

use App\Model\Repository\QuestionRepository;
use App\Model\Repository\AnswerRepository;
use App\Model\Repository\TestQuestionRepository;
use App\Model\Repository\TestQuestionAnswerRepository;
use App\Model\Repository\TestRepository;
use Nextras\Orm\Model\Model;

/**
 * @property-read QuestionRepository $question
 * @property-read AnswerRepository $answer
 * @property-read TestQuestionRepository $testQuestion
 * @property-read TestQuestionAnswerRepository $testQuestionAnswer
 * @property-read TestRepository $test
 */
class Orm extends Model
{
}
