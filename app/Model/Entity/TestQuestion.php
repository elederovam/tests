<?php declare(strict_types=1);

namespace App\Model\Entity;

use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\OneHasMany;

/**
 * @property int                                $id    {primary}
 * @property int                                $rank
 * @property Test                               $test {m:1 Test::$testQuestions}
 * @property OneHasMany|TestQuestionAnswer[]    $testQuestionAnswers {1:m TestQuestionAnswer::$testQuestion, cascade=[persist, remove]}
 * @property Question|null                      $question {m:1 Question, oneSided=true, cascade=[]}
 */
class TestQuestion extends Entity {}
