<?php declare(strict_types=1);

namespace App\Model\Entity;

use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\OneHasMany;

/**
 * @property int                        $id    {primary}
 * @property string                     $code
 * @property OneHasMany|TestQuestion[]  $testQuestions {1:m TestQuestion::$test, cascade=[persist, remove]}
 */
class Test extends Entity {}
