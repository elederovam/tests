<?php declare(strict_types=1);

namespace App\Model\Entity;

use Nextras\Orm\Entity\Entity;

/**
 * @property int                                $id    {primary}
 * @property int                                $rank
 * @property bool                               $selected
 * @property TestQuestion                       $testQuestion {m:1 TestQuestion::$testQuestionAnswers}
 * @property Answer|null                        $answer {m:1 Answer, oneSided=true, cascade=[]}
 */
class TestQuestionAnswer extends Entity {}
