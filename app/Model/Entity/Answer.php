<?php declare(strict_types=1);

namespace App\Model\Entity;

use Nextras\Orm\Entity\Entity;

/**
 * @property int                    $id    {primary}
 * @property string                 $text
 * @property bool                   $correct
 * @property Question               $question {m:1 Question::$answers}
 */
class Answer extends Entity {}
