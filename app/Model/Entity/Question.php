<?php declare(strict_types=1);

namespace App\Model\Entity;

use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\OneHasMany;

/**
 * @property int                    $id    {primary}
 * @property string                 $text
 * @property OneHasMany|Answer[]    $answers {1:m Answer::$question, orderBy=[id, ASC], cascade=[persist, remove]}
 */
class Question extends Entity {
    public const ID_ATTRIBUTE_NAME = 'id';
    public const TEXT_ATTRIBUTE_NAME = 'text';
}
