<?php declare(strict_types=1);

namespace App\Model\Repository;

use App\Model\Entity\Answer;
use Nextras\Orm\Repository\Repository;

class AnswerRepository extends Repository
{
    static function getEntityClassNames(): array
    {
        return [Answer::class];
    }
}
