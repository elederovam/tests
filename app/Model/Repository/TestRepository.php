<?php declare(strict_types=1);

namespace App\Model\Repository;

use App\Model\Entity\Test;
use Nextras\Orm\Repository\Repository;

class TestRepository extends Repository
{
    static function getEntityClassNames(): array
    {
        return [Test::class];
    }
}
