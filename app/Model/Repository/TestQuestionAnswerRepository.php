<?php declare(strict_types=1);

namespace App\Model\Repository;

use App\Model\Entity\TestQuestionAnswer;
use Nextras\Orm\Repository\Repository;

class TestQuestionAnswerRepository extends Repository
{
    static function getEntityClassNames(): array
    {
        return [TestQuestionAnswer::class];
    }
}
