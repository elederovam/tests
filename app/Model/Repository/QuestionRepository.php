<?php declare(strict_types=1);

namespace App\Model\Repository;

use App\Model\Entity\Question;
use Nextras\Orm\Repository\Repository;

class QuestionRepository extends Repository
{
    static function getEntityClassNames(): array
    {
        return [Question::class];
    }
}
