<?php declare(strict_types=1);

namespace App\Model\Repository;

use App\Model\Entity\TestQuestion;
use Nextras\Orm\Repository\Repository;

class TestQuestionRepository extends Repository
{
    static function getEntityClassNames(): array
    {
        return [TestQuestion::class];
    }
}
