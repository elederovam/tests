<?php declare(strict_types=1);

namespace App\AdminModule\DataGrid;

use Tracy\Debugger;
use Tracy\ILogger;
use Ublaboo\DataGrid\DataGrid;

class GridFactory
{
    /**
     * @return DataGrid
     */
    public function create(): DataGrid
    {
        $grid = new DataGrid();
        $grid->setStrictSessionFilterValues(false);
        $grid->setAutoSubmit(false);
        $grid->setItemsPerPageList([10, 20, 50, 100], false);

        return $grid;
    }
}
