<?php declare(strict_types=1);

namespace App\AdminModule\DataGrid\Question;

use Nextras\Orm\Collection\ICollection;

interface IQuestionDataGridFactory
{
    /**
     * @param ICollection $dataSource
     * @return QuestionDataGrid
     */
    public function create(ICollection $dataSource): QuestionDataGrid;
}
