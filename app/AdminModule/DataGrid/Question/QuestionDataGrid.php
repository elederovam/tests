<?php declare(strict_types=1);

namespace App\AdminModule\DataGrid\Question;

use App\AdminModule\DataGrid\GridFactory;
use App\Model\Entity\Question;
use Nette\Application\UI\Control;
use Nextras\Orm\Collection\ICollection;
use Ublaboo\DataGrid\DataGrid;

class QuestionDataGrid extends Control
{
    private ICollection $dataSource;

    private GridFactory $gridFactory;

    /**
     * @param ICollection $dataSource
     * @param GridFactory $gridFactory
     */
    public function __construct(ICollection $dataSource, GridFactory $gridFactory)
    {
        $this->dataSource = $dataSource;
        $this->gridFactory = $gridFactory;
    }

    public function createComponentGrid(): DataGrid
    {
        $grid = $this->gridFactory->create();
        $grid->setDataSource($this->dataSource);

        $grid->addColumnText(Question::TEXT_ATTRIBUTE_NAME, 'Otázka');
        $grid->addAction('questionDetail', 'Zobrazit detail', 'Question:', ['id' => Question::ID_ATTRIBUTE_NAME]);

        return $grid;
    }

    public function render(): void
    {
        $this->getTemplate()->setFile(__DIR__ . DIRECTORY_SEPARATOR . 'questionDataGrid.latte');
        $this->getTemplate()->render();
    }
}
