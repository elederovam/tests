<?php declare(strict_types=1);

namespace App\AdminModule\Forms;

use Nette\Application\UI\Form;

class BasicQuestionEditFormFactory
{
    public const QUESTION_NAME = 'question';
    public const CORRECT_ANSWER_NAME = 'correctAnswer';
    public const WRONG_ANSWER1_NAME = 'wrongAnswer1';
    public const WRONG_ANSWER2_NAME = 'wrongAnswer2';
    public const WRONG_ANSWER3_NAME = 'wrongAnswer3';

    /**
     * Create form with inputs for question, one correct answer and three wrong answers.
     */
    public function createBasicQuestionEditForm(): Form
    {
        $form = new Form();
        $form->addText(self::QUESTION_NAME, 'Otázka')
            ->setRequired();
        $form->addText(self::CORRECT_ANSWER_NAME, 'Správná odpověď')
            ->setRequired();
        $form->addText(self::WRONG_ANSWER1_NAME, 'Nesprávná odpověď 1')
            ->setRequired();
        $form->addText(self::WRONG_ANSWER2_NAME, 'Nesprávná odpověď 2')
            ->setRequired();
        $form->addText(self::WRONG_ANSWER3_NAME, 'Nesprávná odpověď 3')
            ->setRequired();
        $form->addSubmit('submit', 'Uložit')
            ->setHtmlAttribute('class', 'btn btn-primary');

        return $form;
    }
}
