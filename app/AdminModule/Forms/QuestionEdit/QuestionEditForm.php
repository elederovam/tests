<?php declare(strict_types=1);

namespace App\AdminModule\Forms\QuestionEdit;

use App\AdminModule\Forms\BasicQuestionEditFormFactory;
use App\Dto\QuestionDtoWithAnswerIds;
use App\Facade\IQuestionsFacade;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

/**
 * @method void onQuestionSaved(int $questionId)
 */
class QuestionEditForm extends Control
{
    private const CORRECT_ANSWER_ID_NAME = 'cai';
    private const WRONG_ANSWER1_ID_NAME = 'wai1';
    private const WRONG_ANSWER2_ID_NAME = 'wai2';
    private const WRONG_ANSWER3_ID_NAME = 'wai3';

    /**
     * @var callable[]
     */
    public array $onQuestionSaved = [];

    private int $questionId;

    private QuestionDtoWithAnswerIds $questionDto;

    private IQuestionsFacade $questionsFacade;

    private BasicQuestionEditFormFactory $basicQuestionEditFormFactory;

    /**
     * @param int $questionId
     * @param QuestionDtoWithAnswerIds $questionDto
     * @param IQuestionsFacade $questionsFacade
     * @param BasicQuestionEditFormFactory $basicQuestionEditFormFactory
     */
    public function __construct(
        int $questionId,
        QuestionDtoWithAnswerIds $questionDto,
        IQuestionsFacade $questionsFacade,
        BasicQuestionEditFormFactory $basicQuestionEditFormFactory
    )
    {
        $this->questionId = $questionId;
        $this->questionDto = $questionDto;
        $this->questionsFacade = $questionsFacade;
        $this->basicQuestionEditFormFactory = $basicQuestionEditFormFactory;
    }

    public function createComponentForm(): Form
    {
        $form = $this->basicQuestionEditFormFactory->createBasicQuestionEditForm();

        $form->addHidden(self::CORRECT_ANSWER_ID_NAME, $this->questionDto->getCorrectAnswerId());
        $wrongAnswersIds = array_keys($this->questionDto->getWrongAnswers());
        $wrongAnswer1 = $this->questionDto->getWrongAnswers()[$wrongAnswersIds[0]];
        $form->addHidden(self::WRONG_ANSWER1_ID_NAME, $wrongAnswersIds[0]);
        $wrongAnswer2 = $this->questionDto->getWrongAnswers()[$wrongAnswersIds[1]];
        $form->addHidden(self::WRONG_ANSWER2_ID_NAME, $wrongAnswersIds[1]);
        $wrongAnswer3 = $this->questionDto->getWrongAnswers()[$wrongAnswersIds[2]];
        $form->addHidden(self::WRONG_ANSWER3_ID_NAME, $wrongAnswersIds[2]);

        $form->setDefaults([
            BasicQuestionEditFormFactory::QUESTION_NAME => $this->questionDto->getQuestion(),
            BasicQuestionEditFormFactory::CORRECT_ANSWER_NAME => $this->questionDto->getCorrectAnswer(),
            BasicQuestionEditFormFactory::WRONG_ANSWER1_NAME => $wrongAnswer1,
            BasicQuestionEditFormFactory::WRONG_ANSWER2_NAME => $wrongAnswer2,
            BasicQuestionEditFormFactory::WRONG_ANSWER3_NAME => $wrongAnswer3,
        ]);

        $form->onSuccess[] = [$this, 'formSucceeded'];

        return $form;
    }

    public function formSucceeded(Form $form, ArrayHash $values): void
    {
        $updatedQuestionDto = new QuestionDtoWithAnswerIds();
        $updatedQuestionDto->setQuestion($values[BasicQuestionEditFormFactory::QUESTION_NAME]);
        $updatedQuestionDto->setCorrectAnswer($values[BasicQuestionEditFormFactory::CORRECT_ANSWER_NAME]);
        $updatedQuestionDto->setCorrectAnswerId((int) $values[self::CORRECT_ANSWER_ID_NAME]);
        $updatedQuestionDto->addWrongAnswer(
            (int) $values[self::WRONG_ANSWER1_ID_NAME],
            $values[BasicQuestionEditFormFactory::WRONG_ANSWER1_NAME]
        );
        $updatedQuestionDto->addWrongAnswer(
            (int) $values[self::WRONG_ANSWER2_ID_NAME],
            $values[BasicQuestionEditFormFactory::WRONG_ANSWER2_NAME]
        );
        $updatedQuestionDto->addWrongAnswer(
            (int) $values[self::WRONG_ANSWER3_ID_NAME],
            $values[BasicQuestionEditFormFactory::WRONG_ANSWER3_NAME]
        );

        try {
            $this->questionsFacade->updateQuestion($this->questionId, $updatedQuestionDto);
        } catch (\Throwable $exception) {
            $form->addError('Při ukládání otázky nastala chyba.');
            Debugger::log($exception, Debugger::ERROR);
            return;
        }

        $this->onQuestionSaved($this->questionId);
    }

    public function render(): void
    {
        $this->getTemplate()->setFile(__DIR__ . DIRECTORY_SEPARATOR . 'questionEditForm.latte');
        $this->getTemplate()->render();
    }
}
