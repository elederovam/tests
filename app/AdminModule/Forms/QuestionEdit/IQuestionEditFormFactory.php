<?php declare(strict_types=1);

namespace App\AdminModule\Forms\QuestionEdit;

use App\Dto\QuestionDtoWithAnswerIds;

interface IQuestionEditFormFactory
{
    /**
     * @param int $questionId
     * @param QuestionDtoWithAnswerIds $questionDto
     * @return QuestionEditForm
     */
    public function create(int $questionId, QuestionDtoWithAnswerIds $questionDto): QuestionEditForm;
}
