<?php declare(strict_types=1);

namespace App\AdminModule\Forms\QuestionDelete;

use App\Facade\IQuestionsFacade;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

/**
 * @method void onQuestionDeleted()
 */
class QuestionDeleteForm extends Control
{
    /**
     * @var callable[]
     */
    public array $onQuestionDeleted = [];

    private int $questionId;

    private IQuestionsFacade $questionsFacade;

    /**
     * @param int $questionId
     * @param IQuestionsFacade $questionsFacade
     */
    public function __construct(int $questionId, IQuestionsFacade $questionsFacade)
    {
        $this->questionId = $questionId;
        $this->questionsFacade = $questionsFacade;
    }

    public function createComponentForm(): Form
    {
        $form = new Form();
        $form->addSubmit('submit', 'Smazat')
            ->setHtmlAttribute('class', 'btn btn-danger');

        $form->onSuccess[] = [$this, 'formSucceeded'];

        return $form;
    }

    public function formSucceeded(Form $form, ArrayHash $values): void
    {
        try {
            $this->questionsFacade->deleteQuestion($this->questionId);
        } catch (\Throwable $exception) {
            $form->addError('Při mazání otázky nastala chyba.');
            Debugger::log($exception, Debugger::ERROR);
            return;
        }

        $this->onQuestionDeleted();
    }

    public function render(): void
    {
        $this->getTemplate()->setFile(__DIR__ . DIRECTORY_SEPARATOR . 'questionDeleteForm.latte');
        $this->getTemplate()->render();
    }
}
