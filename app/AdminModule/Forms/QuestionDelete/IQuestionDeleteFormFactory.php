<?php declare(strict_types=1);

namespace App\AdminModule\Forms\QuestionDelete;

interface IQuestionDeleteFormFactory
{
    /**
     * @param int $questionId
     * @return QuestionDeleteForm
     */
    public function create(int $questionId): QuestionDeleteForm;
}
