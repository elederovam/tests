<?php declare(strict_types=1);

namespace App\AdminModule\Forms\QuestionNew;

use App\AdminModule\Forms\BasicQuestionEditFormFactory;
use App\Dto\QuestionDto;
use App\Facade\IQuestionsFacade;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

/**
 * @method void onQuestionSaved(int $questionId)
 */
class QuestionNewForm extends Control
{
    /**
     * @var callable[]
     */
    public array $onQuestionSaved = [];

    private IQuestionsFacade $questionsFacade;

    private BasicQuestionEditFormFactory $basicQuestionEditFormFactory;

    /**
     * @param IQuestionsFacade $questionsFacade
     * @param BasicQuestionEditFormFactory $basicQuestionEditFormFactory
     */
    public function __construct(
        IQuestionsFacade $questionsFacade,
        BasicQuestionEditFormFactory $basicQuestionEditFormFactory
    )
    {
        $this->questionsFacade = $questionsFacade;
        $this->basicQuestionEditFormFactory = $basicQuestionEditFormFactory;
    }

    public function createComponentForm(): Form
    {
        $form = $this->basicQuestionEditFormFactory->createBasicQuestionEditForm();

        $form->onSuccess[] = [$this, 'formSucceeded'];

        return $form;
    }

    public function formSucceeded(Form $form, ArrayHash $values): void
    {
        $newQuestionDto = new QuestionDto();
        $newQuestionDto->setQuestion($values[BasicQuestionEditFormFactory::QUESTION_NAME]);
        $newQuestionDto->setCorrectAnswer($values[BasicQuestionEditFormFactory::CORRECT_ANSWER_NAME]);
        $newQuestionDto->addWrongAnswer($values[BasicQuestionEditFormFactory::WRONG_ANSWER1_NAME]);
        $newQuestionDto->addWrongAnswer($values[BasicQuestionEditFormFactory::WRONG_ANSWER2_NAME]);
        $newQuestionDto->addWrongAnswer($values[BasicQuestionEditFormFactory::WRONG_ANSWER3_NAME]);

        try {
            $newQuestionId = $this->questionsFacade->createQuestion($newQuestionDto);
        } catch (\Throwable $exception) {
            $form->addError('Při ukládání otázky nastala chyba.');
            Debugger::log($exception, Debugger::ERROR);
            return;
        }

        $this->onQuestionSaved($newQuestionId);
    }

    public function render(): void
    {
        $this->getTemplate()->setFile(__DIR__ . DIRECTORY_SEPARATOR . 'questionNewForm.latte');
        $this->getTemplate()->render();
    }
}
