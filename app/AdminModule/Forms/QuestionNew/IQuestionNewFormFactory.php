<?php declare(strict_types=1);

namespace App\AdminModule\Forms\QuestionNew;

interface IQuestionNewFormFactory
{
    /**
     * @return QuestionNewForm
     */
    public function create(): QuestionNewForm;
}
