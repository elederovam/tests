<?php declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\AdminModule\DataGrid\Question\IQuestionDataGridFactory;
use App\AdminModule\DataGrid\Question\QuestionDataGrid;
use App\Facade\IQuestionsFacade;

class HomepagePresenter extends BasePresenter
{
    /** @var IQuestionDataGridFactory @inject */
    public IQuestionDataGridFactory $questionDataGridFactory;

    /** @var IQuestionsFacade @inject */
    public IQuestionsFacade $questionsFacade;

    public function createComponentQuestionDataGrid(): QuestionDataGrid
    {
        return $this->questionDataGridFactory->create($this->questionsFacade->getQuestionsCollection());
    }
}
