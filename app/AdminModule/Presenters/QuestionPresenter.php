<?php declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\AdminModule\Forms\QuestionDelete\IQuestionDeleteFormFactory;
use App\AdminModule\Forms\QuestionDelete\QuestionDeleteForm;
use App\Facade\IQuestionsFacade;

class QuestionPresenter extends BasePresenter
{
    /** @var IQuestionsFacade @inject */
    public IQuestionsFacade $questionFacade;

    /** @var IQuestionDeleteFormFactory @inject */
    public IQuestionDeleteFormFactory $questionDeleteFormFactory;

    private int $questionId;

    /**
     * Show detail of question with given ID.
     *
     * @param int $id
     */
    public function actionDefault(int $id): void
    {
        $question = $this->questionFacade->getQuestionDto($id);
        $this->template->setParameters([
            'question' => $question,
            'id' => $id,
        ]);
        $this->questionId = $id;
    }

    public function createComponentQuestionDeleteForm(): QuestionDeleteForm
    {
        $questionDeleteForm = $this->questionDeleteFormFactory->create($this->questionId);
        $questionDeleteForm->onQuestionDeleted[] = function (): void {
            $this->redirect('Homepage:');
        };

        return $questionDeleteForm;
    }
}
