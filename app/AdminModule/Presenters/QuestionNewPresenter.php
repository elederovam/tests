<?php declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\AdminModule\Forms\QuestionNew\IQuestionNewFormFactory;
use App\AdminModule\Forms\QuestionNew\QuestionNewForm;
use App\Facade\IQuestionsFacade;

class QuestionNewPresenter extends BasePresenter
{
    /** @var IQuestionsFacade @inject */
    public IQuestionsFacade $questionFacade;

    /** @var IQuestionNewFormFactory @inject */
    public IQuestionNewFormFactory $questionNewFormFactory;

    public function createComponentQuestionNewForm(): QuestionNewForm
    {
        $questionNewForm = $this->questionNewFormFactory->create();
        $questionNewForm->onQuestionSaved[] = function (int $questionId): void {
            $this->redirect('Question:', $questionId);
        };

        return $questionNewForm;
    }
}
