<?php declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\AdminModule\Forms\QuestionEdit\IQuestionEditFormFactory;
use App\AdminModule\Forms\QuestionEdit\QuestionEditForm;
use App\Dto\QuestionDtoWithAnswerIds;
use App\Facade\IQuestionsFacade;

class QuestionEditPresenter extends BasePresenter
{
    /** @var IQuestionsFacade @inject */
    public IQuestionsFacade $questionFacade;

    /** @var IQuestionEditFormFactory @inject */
    public IQuestionEditFormFactory $questionEditFormFactory;

    private int $questionId;

    private QuestionDtoWithAnswerIds $questionDto;

    /**
     * Show form for updating question with given ID.
     *
     * @param int $id
     */
    public function actionDefault(int $id): void
    {
        $this->template->setParameters([
            'id' => $id,
        ]);
        $question = $this->questionFacade->getQuestionDtoWithAnswerIds($id);
        $this->questionId = $id;
        $this->questionDto = $question;
    }

    public function createComponentQuestionEditForm(): QuestionEditForm
    {
        $questionEditForm = $this->questionEditFormFactory->create($this->questionId, $this->questionDto);
        $questionEditForm->onQuestionSaved[] = function (int $questionId): void {
            $this->redirect('Question:', $questionId);
        };

        return $questionEditForm;
    }
}
