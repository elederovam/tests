<?php declare(strict_types=1);

namespace App\AdminModule\Presenters;

use Nette;

/**
 * Base presenter for AdminModule presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
}
