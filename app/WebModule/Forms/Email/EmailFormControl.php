<?php declare(strict_types=1);

namespace App\WebModule\Forms\Email;

use App\Facade\ITestFacade;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

/**
 * @method void onEmailSend()
 */
class EmailFormControl extends Control
{
    private const EMAIL_NAME = 'email';

    /** @var callable[] */
    public array $onEmailSend = [];

    private ITestFacade $testFacade;

    /**
     * @param ITestFacade $testFacade
     */
    public function __construct(ITestFacade $testFacade)
    {
        $this->testFacade = $testFacade;
    }

    public function createComponentForm(): Form
    {
        $form = new Form();
        $form->addText(self::EMAIL_NAME, 'Email')
            ->setRequired()
            ->addRule(Form::EMAIL, 'Neplatný email');
        $form->addSubmit('submit', 'Odeslat')
            ->setHtmlAttribute('class', 'btn btn-primary');

        $form->onSuccess[] = [$this, 'formSucceeded'];

        return $form;
    }

    public function formSucceeded(Form $form, ArrayHash $values): void
    {
        try {
            $email = $values[self::EMAIL_NAME];
            $this->testFacade->sendEmailWithCode($email);
        } catch (\Throwable $exception) {
            $form->addError('Při odesílání emailu nastala chyba.');
            Debugger::log($exception, Debugger::ERROR);
            return;
        }

        $this->onEmailSend();
    }

    public function render(): void
    {
        $this->getTemplate()->setFile(__DIR__ . DIRECTORY_SEPARATOR . 'emailFormControl.latte');
        $this->getTemplate()->render();
    }
}
