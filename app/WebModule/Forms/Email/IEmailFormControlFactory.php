<?php declare(strict_types=1);

namespace App\WebModule\Forms\Email;

use App\WebModule\Forms\Email\EmailFormControl;

interface IEmailFormControlFactory
{
    /**
     * @return EmailFormControl
     */
    public function create(): EmailFormControl;
}
