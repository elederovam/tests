<?php declare(strict_types=1);

namespace App\WebModule\Presenters;

use Nette;

/**
 * Base presenter for WebModule presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
}
