<?php declare(strict_types=1);

namespace App\WebModule\Presenters;

use App\Model\Orm;
use App\WebModule\Forms\Email\EmailFormControl;
use App\WebModule\Forms\Email\IEmailFormControlFactory;

final class HomepagePresenter extends BasePresenter
{
    /** @var Orm @inject */
    public Orm $orm;

    /** @var IEmailFormControlFactory @inject */
    public IEmailFormControlFactory $emailFormControlFactory;

    public function createComponentEmailFormControl(): EmailFormControl
    {
        $emailFormControl = $this->emailFormControlFactory->create();
        $emailFormControl->onEmailSend[] = function (): void {
            $this->redirect('Test:');
        };

        return $emailFormControl;
	}
}
