<?php declare(strict_types=1);

namespace App\WebModule\Presenters;

use App\WebModule\Forms\Email\IEmailFormControlFactory;

final class TestPresenter extends BasePresenter
{
    /** @var IEmailFormControlFactory @inject */
    public IEmailFormControlFactory $emailFormControlFactory;

    /**
     */
    public function renderDefault(): void
	{

	}

//    public function createComponentEmailFormControl(): EmailFormControl
//    {
//        $emailFormControl = $this->emailFormControlFactory->create();
//        $emailFormControl->onEmailSend[] = function (): void {
//            $this->redirect(':testEntry');
//        };
//
//        return $emailFormControl;
//	}
}
